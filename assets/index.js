function copyClipboard(element) {
    var copied = $("<textarea>");
    $("body").append(copied);
    copied.val($(element).text()).select();
    document.execCommand("copy");
    $(".tooltip-inner").html('Kopyalandı. <i class="fas fa-check-circle"></i>');
    copied.remove();
}